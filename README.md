# Mixie mobile webview showcase application

The application contains only one ViewController. This ViewController have a ```WKWebView``` child. The ViewController must be inherited from UIViewController and must implement the WKUIDelegate protocol.

First we need to initiate a WKWebView:

`let webView: WKWebView = { }()`

For proper web-application integration do the following configurations in the curly brackets:

- To enable javascript execution:

```
let prefs = WKWebpagePreferences()
prefs.allowsContentJavaScript = true
```

- Open attachments in external browser:

```
let wkPreferences = WKPreferences()
wkPreferences.javaScriptCanOpenWindowsAutomatically = true
```

- Create a webview configuration and attach `prefs` and `wkPreferences`

```swift
let configuration = WKWebViewConfiguration()
configuration.defaultWebpagePreferences = prefs
configuration.preferences = wkPreferences
```

- Enable datastore for authentication and media playback for camera handling

```swift
configuration.websiteDataStore = WKWebsiteDataStore.default()
configuration.allowsInlineMediaPlayback = true
configuration.mediaPlaybackRequiresUserActio= false
configuratiorequiresUserActionForMediaPlayback = false
```

- Then initiate the webview and attach configurations:

```swift
let webView = WKWebView(frame: .zero, configuration: configuration)
return webView
```

The complete code:

```swift
let webView: WKWebView = {
        let prefs = WKWebpagePreferences()
        prefs.allowsContentJavaScript = true
        
        let wkPreferences = WKPreferences()
        wkPreferences.javaScriptCanOpenWindowsAutomatically = true
        
        let configuration = WKWebViewConfiguration()
        configuration.defaultWebpagePreferences = prefs
        configuration.preferences = wkPreferences
        configuration.websiteDataStore = WKWebsiteDataStore.default()
        configuration.allowsInlineMediaPlayback = true
        configuration.mediaPlaybackRequiresUserAction = false
        configuration.requiresUserActionForMediaPlayback = false
        let webView = WKWebView(frame: .zero, configuration: configuration)
        return webView
    }()
```

To handle new window opens, we need to implement the `WKUIDelegate`'s `webView` function:

```swift
func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
    if navigationAction.targetFrame == nil, let url = navigationAction.request.url {
            if url.description.lowercased().range(of: "http://") != nil ||
            url.description.lowercased().range(of: "https://") != nil ||
            url.description.lowercased().range(of: "mailto:") != nil {
            UIApplication.shared.openURL(url)
            }
        }
    return nil
}
```

This will be handle new window navigations and open the corresponding application with the `UIApplication`

In the `viewDidLoad` method just add the `webView` to the parent view, load the url, and don't forget to set the uiDelegate to `self`:

```swift
override func viewDidLoad() {
    super.viewDidLoad()
    view.addSubview(webView)
    
    guard let url = URL(string: "your-url-here") else {
        return
    }
    
    webView.uiDelegate = self
    webView.load(URLRequest(url: url))
}
```

Override `viewDidLayoutSubviews` method to fit webview to the window size:

```swift
override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    webView.frame = view.bounds
}
```


# iOS Deep Linking
There are 2 ways of implementing deep linking in IOS: 

- [Custom URL Scheme](https://developer.apple.com/documentation/xcode/defining-a-custom-url-scheme-for-your-app)
- [Universal Links](https://developer.apple.com/documentation/xcode/allowing-apps-and-websites-to-link-to-your-content)

While URL schemes are a well-known way of having deep linking, Universal links are the new way Apple has implemented to easily connect your webpage and your app under the same link

You can read a detailed comparision [here](https://medium.com/wolox/ios-deep-linking-url-scheme-vs-universal-links-50abd3802f97)

# feature/url_scheme_poc

This branch showcases how to setup Custom Url Scheme for your application.

In this example we set up the application with the following Custom Url Scheme:
```
mixie://?redirectTo=/scan/{id}
```
example:
```
mixie://?redirectTo=/scan/931883e1-d411-4bab-9ab0-26493d84fece
```


- Register your URL scheme
    - In XCode: Target→Info tab→URL Types
        - Identifier: your app reverse DNS string
        - URL Schemes: your url scheme id
    
    You can see the changes in your Info.plist:
    ```xml
    <array>
		<dict>
			<key>CFBundleTypeRole</key>
			<string>Editor</string>
			<key>CFBundleURLName</key>
			<string>com.mixie.MixieWV</string>
			<key>CFBundleURLSchemes</key>
			<array>
				<string>mixie</string>
			</array>
		</dict>
	</array>
    ```
- Handle Incoming URLs

    In `SceneDelegate.swift` you can see examples of how to handle incoming URLS with when your up is opened in the background or closed.

    The POC application takes the first query parameter value in the url and saves it to `UserDefaults`

    ``` swift
   // from link when the app was running in background
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        // parse the incoming url and get the redirectTo param value
        guard let urlContext = URLContexts.first,
              let components = NSURLComponents(url: urlContext.url, resolvingAgainstBaseURL: true),
              let redirectTo = components.queryItems?.filter({$0.name == "redirectTo"}).first?.value else {
                  return
              }
        
        // instantiate the ViewController and set the redirectTo value
        if let windowScene = scene as? UIWindowScene {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc : ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            vc.redirectTo = redirectTo
            
            // present the ViewController
            self.window = UIWindow(windowScene: windowScene)
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
    }
    ```

    ```swift
    // from link when app was closed
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // parse the incoming url and get the redirectTo param value
        guard let urlContext = connectionOptions.urlContexts.first,
              let components = NSURLComponents(url: urlContext.url, resolvingAgainstBaseURL: true),
              let redirectTo = components.queryItems?.filter({$0.name == "redirectTo"}).first?.value else {
                  return
              }
        
        // instantiate the ViewController and set the redirectTo value
        if let windowScene = scene as? UIWindowScene {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc : ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            vc.redirectTo = redirectTo
            
            // present the ViewController
            self.window = UIWindow(windowScene: windowScene)
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }

    ```

    In the ViewController-s viewDidLoad method we simply construct the url with the redirectTo value or the defaultRedirect value and present the webview

    ```swift
    override func viewDidLoad() {
        var urlString = appUrl
        if let redirectTo = redirectTo {
            urlString += redirectTo
        } else {
            urlString += defaultRedirect
        }
        ...
    ```

- Testing
    - Install the application
    - In Safari enter the following url :  `mixie://?redirectTo=/scan/931883e1-d411-4bab-9ab0-26493d84fece`


# feature/universal_link_poc

This branch showcases how to setup Universal Links for your application.


- Setup Supporting Associated Domains in your website
    - Add the Associated Domain File to Your Website
        - Create a file named `apple-app-site-association`
        - The following JSON code represents the contents of a simple association file. With this configuration we link all paths in our domain to the `com.mixie.MixieWV` application. The appId is in the following format: `<Application Identifier Prefix>.<Bundle Identifier>
`
            
            ```json
            {
                "applinks": {
                    "apps": [],
                    "details": [
                        {
                            "appID": "89UUS5ME2C.com.mixie.MixieWV",
                            "paths": [ "*"]
                        }
                        
                    ]
                }
            }
            ```
        - Place it in your site’s .`well-known` directory. The file’s URL should match the following format:
            
            `https://<fully qualified domain>/.well-known/apple-app-site-association`
- Setup the Associated Domains Entitlement to Your App
    - In XCode: Target→Signing & Capabilities→+→Associated Domains 

        Each domain you specify uses the following format:
        `<service>:<fully qualified domain>`
        In the example: `applinks:mixie.luna-space.com`

- Handle Incoming URLs
    - In your AppDelegate or UIWindowSceneDelegate override the following methods:
        
    ```swift
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        guard let url = userActivity.webpageURL,
              let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true)  else {
                  return
              }
        print(components.queryItems?.first?.value)
    }
    ```

- Testing
    - Install the application
    - In Safari enter the site with the path you configured in `apple-app-site-association` 
    - You should see an Open banner

# Important

- Both solution won't work, if your application is not installed.
- Fallback to AppStore is not supported.
- Universal Links also requires your app to be published in the AppStore and HTTPS for your link tracking domain


# File download

The ViewController must implement 2 protocols:

- WKNavigationDelegate

Determine whether to initiate a download or load a webpage.

```
func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, preferences: WKWebpagePreferences, decisionHandler: @escaping (WKNavigationActionPolicy, WKWebpagePreferences) -> Void) {
        if navigationAction.shouldPerformDownload {
            decisionHandler(.download, preferences)
        } else {
            decisionHandler(.allow, preferences)
        }
}
```

```
func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        if navigationResponse.canShowMIMEType {
            decisionHandler(.allow)
        } else {
            decisionHandler(.download)
        }
}
```

Set the delegate

```
func webView(_ webView: WKWebView, navigationAction: WKNavigationAction, didBecome download: WKDownload) {
        download.delegate = self
}
```

```
func webView(_ webView: WKWebView, navigationResponse: WKNavigationResponse, didBecome download: WKDownload) {
        download.delegate = self
}
```

- WKDownloadDelegate

 We can't save files right after downloading, we follow a two-step process.
First, we save the downloaded content to a temporary directory.
Then, we keep track of the path to this content.
When the download is finished, we can open the file using the stored path in the downloadDidFinish function.

```
func download(_ download: WKDownload, decideDestinationUsing response: URLResponse, suggestedFilename: String, completionHandler: @escaping (URL?) -> Void) {
        let documentDirectory = NSTemporaryDirectory()
        let fileUrl = documentDirectory.appending("\(suggestedFilename)")
        path = URL(fileURLWithPath: fileUrl)
        completionHandler(path)
    }
```
    
Display the activity view controller to let the user select the desired action to perform on the file, such as saving or sharing.

```
    func downloadDidFinish(_ download: WKDownload) {
        let activityViewController = UIActivityViewController(activityItems: [path], applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
```
    
Implement error handling

```
func download(_ download: WKDownload, didFailWithError error: Error, resumeData: Data?) {
    // TODO: handle error
}
```

Finally we set the delegate for the webview

```
webView.navigationDelegate = self
```
