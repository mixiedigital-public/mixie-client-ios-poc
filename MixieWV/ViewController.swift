//
//  ViewController.swift
//  MixieWV
//
//  Created by Ignácz Bence on 2021. 08. 31..
//

import UIKit
import WebKit

class ViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, WKDownloadDelegate {
    var path: URL?
    
    // We can't save files right after downloading, we follow a two-step process.
    // First, we save the downloaded content to a temporary directory.
    // Then, we keep track of the path to this content.
    // When the download is finished, we can open the file using the stored path in the downloadDidFinish function.
    func download(_ download: WKDownload, decideDestinationUsing response: URLResponse, suggestedFilename: String, completionHandler: @escaping (URL?) -> Void) {
        let documentDirectory = NSTemporaryDirectory()
        let fileUrl = documentDirectory.appending("\(suggestedFilename)")
        path = URL(fileURLWithPath: fileUrl)
        completionHandler(path)
    }
    
    // Display the activity view controller to let the user select the desired action to perform on the file, such as saving or sharing.
    func downloadDidFinish(_ download: WKDownload) {
        let activityViewController = UIActivityViewController(activityItems: [path], applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
    
    func download(_ download: WKDownload, didFailWithError error: Error, resumeData: Data?) {
        // TODO: handle error
    }
    
    // Determine whether to initiate a download or load a webpage.
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, preferences: WKWebpagePreferences, decisionHandler: @escaping (WKNavigationActionPolicy, WKWebpagePreferences) -> Void) {
        if navigationAction.shouldPerformDownload {
            decisionHandler(.download, preferences)
        } else {
            decisionHandler(.allow, preferences)
        }
    }

    // Determine whether to initiate a download or load a webpage.
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        if navigationResponse.canShowMIMEType {
            decisionHandler(.allow)
        } else {
            decisionHandler(.download)
        }
    }
    
    // Set the download delegate
    func webView(_ webView: WKWebView, navigationAction: WKNavigationAction, didBecome download: WKDownload) {
        download.delegate = self
    }
    
    // Set the download delegate
    func webView(_ webView: WKWebView, navigationResponse: WKNavigationResponse, didBecome download: WKDownload) {
        download.delegate = self
    }
    

    let webView: WKWebView = {
        let prefs = WKWebpagePreferences()
        prefs.allowsContentJavaScript = true
        
        let wkPreferences = WKPreferences()
        wkPreferences.javaScriptCanOpenWindowsAutomatically = true
        
        let configuration = WKWebViewConfiguration()
        configuration.defaultWebpagePreferences = prefs
        configuration.preferences = wkPreferences
        configuration.websiteDataStore = WKWebsiteDataStore.default()
        configuration.allowsInlineMediaPlayback = true
        configuration.mediaPlaybackRequiresUserAction = false
        configuration.requiresUserActionForMediaPlayback = false
        let webView = WKWebView(frame: .zero, configuration: configuration)
        return webView
    }()
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil, let url = navigationAction.request.url {
              if url.description.lowercased().range(of: "http://") != nil ||
                url.description.lowercased().range(of: "https://") != nil ||
                url.description.lowercased().range(of: "mailto:") != nil {
                UIApplication.shared.openURL(url)
              }
            }
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(webView)
        
        guard let url = URL(string: "https://app-testing.mixiedigital.com/token?data=eyJhbGciOiJIUzI1NiIsImRpc2NyaW1pbmF0b3IiOiJTSU1QTEUifQ.eyJzdWIiOiJ7XG4gIFwiZW1haWxcIjogXCJjc2ludGFsYW4uYmFsYXpzK2ludGVncmF0ZWRwb255dmlzaXRvckBkYW51Yml1c2luZm8uaHVcIixcbiAgXCJhdXRoZW50aWNhdGlvbk93bmVyRW1haWxcIjogXCJjc2ludGFsYW4uYmFsYXpzK3Byb3VkcG9uaWVzQGRhbnViaXVzaW5mby5odVwiXG59IiwiaXNzIjoibWl4aWVkaWdpdGFsLmNvbSIsImV4cCI6MTY1NDc4NDc0MywiaWF0IjoxNjQ3MDA4NzQzfQ.VuZp2HfPC0o4VXSJ1m2w5VOH01OlpqxX6Yke7yBPhBU&redirectTo=%2Fscanner") else {
            return
        }
        
        webView.uiDelegate = self
        // set the navigation delegate for the webview
        webView.navigationDelegate = self
        webView.load(URLRequest(url: url))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        webView.frame = view.bounds
    }


}

